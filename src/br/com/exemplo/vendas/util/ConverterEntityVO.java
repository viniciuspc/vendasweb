package br.com.exemplo.vendas.util;

import br.com.exemplo.vendas.negocio.entity.Cliente;
import br.com.exemplo.vendas.negocio.entity.ClienteFisico;
import br.com.exemplo.vendas.negocio.entity.ClienteJuridico;
import br.com.exemplo.vendas.negocio.entity.Compra;
import br.com.exemplo.vendas.negocio.entity.Item;
import br.com.exemplo.vendas.negocio.entity.Produto;
import br.com.exemplo.vendas.negocio.entity.Reserva;
import br.com.exemplo.vendas.negocio.model.vo.ClienteFisicoVO;
import br.com.exemplo.vendas.negocio.model.vo.ClienteJuridicoVO;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.negocio.model.vo.ItemVO;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.negocio.model.vo.ReservaVO;

public class ConverterEntityVO {
	
	private static ConverterEntityVO instancia;
	
	private ConverterEntityVO(){
		
	}
	
	public static ConverterEntityVO getInstancia(){
		if(instancia == null){
			instancia = new ConverterEntityVO();
		}
		
		return instancia;
	}
	
	public ClienteVO converteClienteEtityVo(Cliente cliente) {
		ClienteVO clienteVO;

		if(cliente instanceof ClienteFisico){
			clienteVO = new ClienteFisicoVO(cliente.getUsuario().getLogin(), cliente.getNome(), cliente.getEndereco(), cliente.getTelefone(), cliente.getSituacao(), ((ClienteFisico) cliente).getRg(), ((ClienteFisico) cliente).getCpf());
		} else {
			clienteVO = new ClienteJuridicoVO(cliente.getUsuario().getLogin(), cliente.getNome(), cliente.getEndereco(), cliente.getTelefone(), cliente.getSituacao(), ((ClienteJuridico) cliente).getCnpj(), ((ClienteJuridico) cliente).getIe());
		}
		return clienteVO;
	}

	public ReservaVO converteReservaEntityVO(Reserva reserva) {
		ReservaVO reservaVO = new ReservaVO(reserva.getCodigo(), reserva.getDate(), reserva.getAtendente(), reserva.getSituacao(), reserva.getValor(), reserva.getCliente().getCodigo());
		return reservaVO;
	}

	public CompraVO converteCompraEntityVO(Compra compra) {
		Long compraCodigo = null;
		if(compra.getReserva()!=null){
			compraCodigo = compra.getReserva().getCodigo();
		}
		
		Long clienteCodigo = null;
		if(compra.getCliente() != null){
			clienteCodigo = compra.getCliente().getCodigo();
		}
		CompraVO compraVO = new CompraVO(compra.getNumero(), compra.getData(), compra.getResponsavel(), compra.getSituacao(), compra.getValor(), compraCodigo, clienteCodigo);
		return compraVO;
	}

	public ItemVO converteItemEntityVO(Item item) {
		ItemVO itemVo = new ItemVO(item.getId(), item.getQuantidade(), item.getValor(), item.getSituacao() , item.getReserva().getCodigo(), item.getCompra().getNumero(), item.getProduto().getCodigo());
		return itemVo;
	}
	
	public ProdutoVO converterProdutoEntityVO(Produto produto){
		ProdutoVO produtoVO = new ProdutoVO(produto.getCodigo(), produto.getDescricao(), produto.getPreco(), produto.getEstoque());
		return produtoVO;
	}
}
