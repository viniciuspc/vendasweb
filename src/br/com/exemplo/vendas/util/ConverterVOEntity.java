package br.com.exemplo.vendas.util;

import br.com.exemplo.vendas.negocio.entity.Cliente;
import br.com.exemplo.vendas.negocio.entity.ClienteFisico;
import br.com.exemplo.vendas.negocio.entity.ClienteJuridico;
import br.com.exemplo.vendas.negocio.entity.Compra;
import br.com.exemplo.vendas.negocio.entity.Item;
import br.com.exemplo.vendas.negocio.entity.Produto;
import br.com.exemplo.vendas.negocio.entity.Reserva;
import br.com.exemplo.vendas.negocio.entity.Usuario;
import br.com.exemplo.vendas.negocio.model.vo.ClienteFisicoVO;
import br.com.exemplo.vendas.negocio.model.vo.ClienteJuridicoVO;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.negocio.model.vo.ItemVO;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.negocio.model.vo.ReservaVO;

public class ConverterVOEntity {
	
private static ConverterVOEntity instancia;
	
	private ConverterVOEntity(){
		
	}
	
	public static ConverterVOEntity getInstancia(){
		if(instancia == null){
			instancia = new ConverterVOEntity();
		}
		
		return instancia;
	}
	
	public Compra converteCompraVOEntity(CompraVO compraVO, Cliente cliente, Reserva reserva){
		Compra compra = new Compra(compraVO.getNumero(), compraVO.getData(), compraVO.getResponsavel(), compraVO.getSituacao(), compraVO.getValor(), reserva, cliente);
		return compra;
	}
	
	public Reserva converteReservaVOEntity(ReservaVO reservaVO, Cliente cliente){
		Reserva reserva = new Reserva(reservaVO.getCodigo(), reservaVO.getDate(), reservaVO.getAtendente(), reservaVO.getSituacao(), reservaVO.getValor(), cliente);
		return reserva;
	}
	
	public Item converteItemVOEntity(ItemVO itemVO, Compra compra, Reserva reserva, Produto produto){
		Item item = new Item(itemVO.getId(), itemVO.getQuantidade(), itemVO.getValor(), itemVO.getSituacao(), reserva, compra, produto);
		return item;
	}
	
	public Cliente converteClienteVOEntity(ClienteVO clienteVO, Usuario usuario) {
		Cliente cliente;
		if (clienteVO instanceof ClienteFisicoVO) {
			cliente = new ClienteFisico(clienteVO.getNome(), clienteVO.getEndereco(),
					clienteVO.getTelefone(), clienteVO.getSituacao(), usuario,
					((ClienteFisicoVO) clienteVO).getRg(),
					((ClienteFisicoVO) clienteVO).getCpf());
		} else {
			cliente = new ClienteJuridico(clienteVO.getNome(), clienteVO.getEndereco(),
					clienteVO.getTelefone(), clienteVO.getSituacao(), usuario,
					((ClienteJuridicoVO) clienteVO).getCnpj(),
					((ClienteJuridicoVO) clienteVO).getIe());
		}
		return cliente;
	}
	
	public Produto converteProdutoVOEntity(ProdutoVO produtoVO){
		Produto produto = new Produto(produtoVO.getCodigo(), produtoVO.getDescricao(), produtoVO.getPreco(), produtoVO.getEstoque());
		return produto;
	}

}
