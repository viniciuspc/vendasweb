package br.com.exemplo.vendas.negocio.entity;

import javax.persistence.Entity;

@Entity
public class ClienteFisico extends Cliente {
	
	private static final long serialVersionUID = -485026844095491014L;
	
	private String rg;
	private String cpf;
	
	public ClienteFisico(String nome, String endereco, String telefone,
			String situacao, Usuario usuario, String rg, String cpf) {
		super(nome, endereco, telefone, situacao, usuario);
		
		this.rg = rg;
		this.cpf = cpf;
	}
	
	public ClienteFisico() {
		super();
	}
	
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	

}
