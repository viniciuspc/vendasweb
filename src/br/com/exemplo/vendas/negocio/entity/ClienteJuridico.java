package br.com.exemplo.vendas.negocio.entity;

import javax.persistence.Entity;

@Entity
public class ClienteJuridico extends Cliente {
	
	private static final long serialVersionUID = 5311362973326314849L;
	
	private String cnpj;
	private String ie;
	
	public ClienteJuridico(String nome, String endereco, String telefone,
			String situacao, Usuario usuario, String cnpj, String ie) {
		super(nome, endereco, telefone, situacao, usuario);
		this.cnpj = cnpj;
		this.ie = ie;
	}
	
	public ClienteJuridico() {
		super();
	}
	
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getIe() {
		return ie;
	}
	public void setIe(String ie) {
		this.ie = ie;
	}
}
