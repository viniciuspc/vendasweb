package br.com.exemplo.vendas.negocio.interfaces;

public interface PreparaBaseDadosInterface {
	
	public void popularBaseDados() throws br.com.exemplo.vendas.util.exception.LayerException, java.rmi.RemoteException;

}
