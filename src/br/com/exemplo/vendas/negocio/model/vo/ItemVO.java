package br.com.exemplo.vendas.negocio.model.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class ItemVO implements Serializable {
	
	private static final long serialVersionUID = 6101053721081934546L;
	
	private Long id;
	
	private Long quantidade;
	private BigDecimal valor;
	private String situacao;
	private Long reservaCodigo;
	private Long compraNumero;
	private Long produtoCodigo;
	
	public ItemVO(Long id, Long quantidade, BigDecimal valor, String situacao,
			Long reservaCodigo, Long compraNumero, Long produtoCodigo) {
		super();
		this.id = id;
		this.quantidade = quantidade;
		this.valor = valor;
		this.situacao = situacao;
		this.reservaCodigo = reservaCodigo;
		this.compraNumero = compraNumero;
		this.produtoCodigo = produtoCodigo;
	}
	public ItemVO() {
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public Long getReservaCodigo() {
		return reservaCodigo;
	}
	public void setReservaCodigo(Long reservaCodigo) {
		this.reservaCodigo = reservaCodigo;
	}
	public Long getCompraNumero() {
		return compraNumero;
	}
	public void setCompraNumero(Long compraNumero) {
		this.compraNumero = compraNumero;
	}
	public Long getProdutoCodigo() {
		return produtoCodigo;
	}
	public void setProdutoCodigo(Long produtoCodigo) {
		this.produtoCodigo = produtoCodigo;
	}
	
	@Override
	public String toString() {
		return "ItemVO [id=" + id + ", quantidade=" + quantidade + ", valor="
				+ valor + ", situacao=" + situacao + ", reservaCodigo="
				+ reservaCodigo + ", compraNumero=" + compraNumero
				+ ", produtoCodigo=" + produtoCodigo + "]";
	}
	
	
}
