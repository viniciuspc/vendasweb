package br.com.exemplo.vendas.negocio.model.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CompraVO  implements Serializable {
	
	private static final long serialVersionUID = -6754358763159609140L;
	
	private Long numero;
	private Date data;
	private String responsavel;
	private String situacao;
	private BigDecimal valor;
	
	private Long reservaCodigo;
	private Long clienteCodigo;
	public CompraVO(Long numero, Date data, String responsavel,
			String situacao, BigDecimal valor, Long reservaCodigo,
			Long clienteCodigo) {
		super();
		this.numero = numero;
		this.data = data;
		this.responsavel = responsavel;
		this.situacao = situacao;
		this.valor = valor;
		this.reservaCodigo = reservaCodigo;
		this.clienteCodigo = clienteCodigo;
	}
	
	public CompraVO() {
	}
	
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public Long getReservaCodigo() {
		return reservaCodigo;
	}
	public void setReservaCodigo(Long reservaCodigo) {
		this.reservaCodigo = reservaCodigo;
	}
	public Long getClienteCodigo() {
		return clienteCodigo;
	}
	public void setClienteCodigo(Long clienteCodigo) {
		this.clienteCodigo = clienteCodigo;
	}
	@Override
	public String toString() {
		return "CompraVO [numero=" + numero + ", data=" + data
				+ ", responsavel=" + responsavel + ", situacao=" + situacao
				+ ", valor=" + valor + ", reservaCodigo=" + reservaCodigo
				+ ", clienteCodigo=" + clienteCodigo + "]";
	}
	
	
}
