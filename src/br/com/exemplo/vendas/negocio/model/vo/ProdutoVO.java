package br.com.exemplo.vendas.negocio.model.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashSet;
import java.util.Set;

public class ProdutoVO implements Serializable {
	
	private static final long serialVersionUID = 6979256745533037498L;
	
	private Long codigo;
	private String descricao;
	private BigDecimal preco;
	private Long estoque;
	
	public ProdutoVO(Long codigo, String descricao, BigDecimal preco,
			Long estoque) {
		super();
		this.codigo = codigo;
		this.descricao = descricao;
		this.preco = preco;
		this.estoque = estoque;
	}
	
	public ProdutoVO() {
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public Long getEstoque() {
		return estoque;
	}

	public void setEstoque(Long estoque) {
		this.estoque = estoque;
	}

	@Override
	public String toString() {
		return "ProdutoVO [codigo=" + codigo + ", descricao=" + descricao
				+ ", preco=" + preco + ", estoque=" + estoque + "]";
	}
	
	
}
