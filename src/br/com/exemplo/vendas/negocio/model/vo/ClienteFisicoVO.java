package br.com.exemplo.vendas.negocio.model.vo;


public class ClienteFisicoVO extends ClienteVO {

	private static final long serialVersionUID = -1319743565427118670L;
	
	private String rg;
	private String cpf;
	
	public ClienteFisicoVO(String usuarioLogin, String nome,
			String endereco, String telefone, String situacao, String rg,
			String cpf) {
		super(usuarioLogin, nome, endereco, telefone, situacao );
		this.rg = rg;
		this.cpf = cpf;
	}
	
	public ClienteFisicoVO() {
	}


	public String getRg() {
		return rg;
	}


	public void setRg(String rg) {
		this.rg = rg;
	}


	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	@Override
	public String toString() {
		return "ClienteFisicoVO [getLogin()="
				+ getUsuarioLogin() + ", getCodigo()=" + getCodigo() + ", getNome()="
				+ getNome() + ", getEndereco()=" + getEndereco()
				+ ", getTelefone()=" + getTelefone() + ", getSituacao()="
				+ getSituacao() + ", rg=" + rg + ", cpf=" + cpf + "]";
	}
	
}
