package br.com.exemplo.vendas.negocio.model.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class ReservaVO implements Serializable {
	
	private static final long serialVersionUID = -4823641108122572655L;
	
	private Long codigo;
	private Date date;
	private String atendente;
	private String situacao;
	private BigDecimal valor;
	
	private Long clienteCodigo;
	
	public ReservaVO(Long codigo, Date date, String atendente, String situacao,
			BigDecimal valor, Long clienteCodigo) {
		super();
		this.codigo = codigo;
		this.date = date;
		this.atendente = atendente;
		this.situacao = situacao;
		this.valor = valor;
		this.clienteCodigo = clienteCodigo;
	}
	
	public ReservaVO() {
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAtendente() {
		return atendente;
	}

	public void setAtendente(String atendente) {
		this.atendente = atendente;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public Long getClienteCodigo() {
		return clienteCodigo;
	}

	public void setClienteCodigo(Long clienteCodigo) {
		this.clienteCodigo = clienteCodigo;
	}

	@Override
	public String toString() {
		return "ReservaVO [codigo=" + codigo + ", date=" + date
				+ ", atendente=" + atendente + ", situacao=" + situacao
				+ ", valor=" + valor + ", clienteCodigo=" + clienteCodigo + "]";
	}
	
}
