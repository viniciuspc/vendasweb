package br.com.exemplo.vendas.negocio.model.vo;


public class ClienteJuridicoVO extends ClienteVO {
	
	private static final long serialVersionUID = -2347969856431995686L;
	
	private String cnpj;
	private String ie;
	
	
	public ClienteJuridicoVO(String usuarioLogin, String nome,
			String endereco, String telefone, String situacao, String cnpj,
			String ie) {
		super(usuarioLogin, nome, endereco, telefone, situacao);
		this.cnpj = cnpj;
		this.ie = ie;
	}
	
	public ClienteJuridicoVO() {
	}


	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getIe() {
		return ie;
	}
	public void setIe(String ie) {
		this.ie = ie;
	}


	@Override
	public String toString() {
		return "ClienteJuridicoVO [getLogin()=" + getUsuarioLogin() + ", getCodigo()=" + getCodigo()
				+ ", getNome()=" + getNome() + ", getEndereco()="
				+ getEndereco() + ", getTelefone()=" + getTelefone()
				+ ", getSituacao()=" + getSituacao() + " cnpj=" + cnpj + ", ie=" + ie + "]";
	}
	
	

}
