package br.com.exemplo.vendas.negocio.ejb;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.exemplo.vendas.negocio.dao.DaoFactory;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ProdutoLocal;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ProdutoRemote;
import br.com.exemplo.vendas.negocio.entity.Produto;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.util.ConverterEntityVO;
import br.com.exemplo.vendas.util.ConverterVOEntity;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

@Stateless
public class ProdutoBean implements ProdutoLocal, ProdutoRemote {
	
	@PersistenceContext(unitName = "Vendas")
	EntityManager em;

	@Override
	public ServiceDTO inserirProduto(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		ProdutoVO produtoVO = (ProdutoVO) requestDTO.get("produtoVO");
		if(produtoVO != null){
			Produto produto = ConverterVOEntity.getInstancia().converteProdutoVOEntity(produtoVO);
			if (DaoFactory.getProdutoDAO(em).inserir(produto)) {
				responseDTO.set("resposta", new Boolean(true));
			} else {
				responseDTO.set("resposta", new Boolean(false));
			}
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO selecionarTodosProdutos(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		Produto produto = null;
		List<Produto> lista = DaoFactory.getProdutoDAO(em).listar();
		if(lista!=null && !lista.isEmpty()){
			ProdutoVO[] produtos = new ProdutoVO[lista.size()];
			for(int i = 0; i<lista.size();i++){
				produto = lista.get(i);
				ProdutoVO produtoVO = ConverterEntityVO.getInstancia().converterProdutoEntityVO(produto);
				produtos[i] = produtoVO;
			}
			responseDTO.set("listaProdutos", produtos);
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO selecionarProdutosComValorEstoque(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		Produto produto = null;
		List<Produto> lista = DaoFactory.getProdutoDAO(em).listarComValorEstoque(new BigDecimal(1000), 2L);
		if(lista!=null && !lista.isEmpty()){
			ProdutoVO[] produtos = new ProdutoVO[lista.size()];
			for(int i = 0; i<lista.size();i++){
				produto = lista.get(i);
				ProdutoVO produtoVO = ConverterEntityVO.getInstancia().converterProdutoEntityVO(produto);
				produtos[i] = produtoVO;
			}
			responseDTO.set("listaProdutos", produtos);
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO alterarProduto(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		ProdutoVO produtoVO = (ProdutoVO) requestDTO.get("produtoVO");
		if(produtoVO != null){
			
			Produto produto = ConverterVOEntity.getInstancia().converteProdutoVOEntity(produtoVO);
			if (DaoFactory.getProdutoDAO(em).alterar(produto)) {
				responseDTO.set("resposta", new Boolean(true));
			} else {
				responseDTO.set("resposta", new Boolean(false));
			}
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO excluirProduto(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		ProdutoVO vo = (ProdutoVO) requestDTO.get("usuarioVO");
		if (vo != null) {
			Produto produto = new Produto();
			produto.setCodigo(vo.getCodigo());
			if (DaoFactory.getProdutoDAO(em).excluir(produto)) {
				responseDTO.set("resposta", new Boolean(true));
			} else {
				responseDTO.set("resposta", new Boolean(false));
			}
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO getProduto(ServiceDTO requestDTO) throws LayerException,
			RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		ProdutoVO produtoVO = (ProdutoVO) requestDTO.get("produtoVO");
		if(produtoVO != null){
			Produto produto = DaoFactory.getProdutoDAO(em).localizar(produtoVO.getCodigo());
			if(produto != null){
				ProdutoVO getProduto = ConverterEntityVO.getInstancia().converterProdutoEntityVO(produto);
				responseDTO.set("getProduto", getProduto);
			}
		}
		return responseDTO;
	}
	
	

}
