package br.com.exemplo.vendas.negocio.ejb;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.exemplo.vendas.negocio.dao.DaoFactory;
import br.com.exemplo.vendas.negocio.ejb.interfaces.CompraLocal;
import br.com.exemplo.vendas.negocio.ejb.interfaces.CompraRemote;
import br.com.exemplo.vendas.negocio.entity.Cliente;
import br.com.exemplo.vendas.negocio.entity.Compra;
import br.com.exemplo.vendas.negocio.entity.Reserva;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.util.ConverterEntityVO;
import br.com.exemplo.vendas.util.ConverterVOEntity;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

@Stateless
public class CompraBean implements CompraLocal, CompraRemote {
	
	@PersistenceContext(unitName = "Vendas")
	EntityManager em;

	@Override
	public ServiceDTO inserirCompra(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		CompraVO compraVO = (CompraVO) requestDTO.get("compraVO");
		if(compraVO != null){
			Cliente cliente = null;
			if(compraVO.getClienteCodigo() != null){
				cliente = DaoFactory.getClienteDAO(em).localizar(compraVO.getClienteCodigo());
			}
			
			Reserva reserva = null;
			if(compraVO.getReservaCodigo() != null){
				reserva = DaoFactory.getReservaDAO(em).localizar(compraVO.getReservaCodigo());
			}
			
			Compra compra = ConverterVOEntity.getInstancia().converteCompraVOEntity(compraVO, cliente, reserva);
			
			if (DaoFactory.getCompraDAO(em).inserir(compra)) {
				responseDTO.set("resposta", new Boolean(true));
			} else {
				responseDTO.set("resposta", new Boolean(false));
			}
			
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO selecionarTodasCompras(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		Compra compra = null;
		List<Compra> lista = DaoFactory.getCompraDAO(em).listar();
		if(lista!=null && !lista.isEmpty()){
			CompraVO[] compras = new CompraVO[lista.size()];
			for(int i = 0; i<lista.size();i++){
				compra = lista.get(i);
				CompraVO compraVO = ConverterEntityVO.getInstancia().converteCompraEntityVO(compra);
				compras[i] = compraVO;
			}
			responseDTO.set("listaCompras", compras);
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO selecionarComprasEntreValores(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		Compra compra = null;
		List<Compra> lista = DaoFactory.getCompraDAO(em).listarComValor(new BigDecimal(0), new BigDecimal(500));
		if(lista!=null && !lista.isEmpty()){
			CompraVO[] compras = new CompraVO[lista.size()];
			for(int i = 0; i<lista.size();i++){
				compra = lista.get(i);
				CompraVO compraVO = ConverterEntityVO.getInstancia().converteCompraEntityVO(compra);
				compras[i] = compraVO;
			}
			responseDTO.set("listaCompras", compras);
		}
		return responseDTO;
	}
	
	@Override
	public ServiceDTO selecionarComprasComReserva(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		Compra compra = null;
		List<Compra> lista = DaoFactory.getCompraDAO(em).listarComReserva();
		if(lista!=null && !lista.isEmpty()){
			CompraVO[] compras = new CompraVO[lista.size()];
			for(int i = 0; i<lista.size();i++){
				compra = lista.get(i);
				CompraVO compraVO = ConverterEntityVO.getInstancia().converteCompraEntityVO(compra);
				compras[i] = compraVO;
			}
			responseDTO.set("listaCompras", compras);
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO alterarCompra(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		CompraVO compraVO = (CompraVO) requestDTO.get("compraVO");
		if(compraVO != null){
			
			Cliente cliente = DaoFactory.getClienteDAO(em).localizar(compraVO.getClienteCodigo());
			Reserva reserva = DaoFactory.getReservaDAO(em).localizar(compraVO.getReservaCodigo());
			
			Compra compra = ConverterVOEntity.getInstancia().converteCompraVOEntity(compraVO, cliente, reserva);
			
			if (DaoFactory.getCompraDAO(em).alterar(compra)) {
				responseDTO.set("resposta", new Boolean(true));
			} else {
				responseDTO.set("resposta", new Boolean(false));
			}
			
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO excluirCompra(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		
		ServiceDTO responseDTO = new ServiceDTO();
		CompraVO compraVO = (CompraVO) requestDTO.get("compraVO");
		if(compraVO != null){
			Compra compra = new Compra();
			compra.setNumero(compraVO.getNumero());
			if (DaoFactory.getCompraDAO(em).excluir(compra)) {
				responseDTO.set("resposta", new Boolean(true));
			} else {
				responseDTO.set("resposta", new Boolean(false));
			}
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO getCompra(ServiceDTO requestDTO) throws LayerException,
			RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		CompraVO compraVO = (CompraVO) requestDTO.get("compraVO");
		if(compraVO != null){
			Compra compra = DaoFactory.getCompraDAO(em).localizar(compraVO.getNumero());
			if(compra != null){
				CompraVO getCompra = ConverterEntityVO.getInstancia().converteCompraEntityVO(compra);
				responseDTO.set("getCompra", getCompra);
			}
		}
		
		return responseDTO;
	}

	
}
