/**************************************************
 * Sistema de Vendas
 * Trabalho de avaliacao final da
 * disciplina Tecnologia Webservices e Restful
 * FIAP Turma 18SCJ
 * Artur Diniz Samora   -   RM 42934
 * Jose Roberto Salinas -   RM 42937
 ***************************************************/

package br.com.exemplo.vendas.negocio.ejb.client ;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date ;
import java.util.HashSet;
import java.util.Hashtable ;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.naming.Context ;
import javax.naming.InitialContext ;
import javax.naming.NamingException;

import br.com.exemplo.vendas.negocio.interfaces.ClienteInterface;
import br.com.exemplo.vendas.negocio.interfaces.CompraInterface;
import br.com.exemplo.vendas.negocio.interfaces.ProdutoInterface;
import br.com.exemplo.vendas.negocio.interfaces.ReservaInterface;
import br.com.exemplo.vendas.negocio.interfaces.UsuarioInterface ;
import br.com.exemplo.vendas.negocio.model.vo.ClienteFisicoVO;
import br.com.exemplo.vendas.negocio.model.vo.ClienteJuridicoVO;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.negocio.model.vo.ItemVO;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.negocio.model.vo.ReservaVO;
import br.com.exemplo.vendas.negocio.model.vo.UsuarioVO ;
import br.com.exemplo.vendas.util.dto.ServiceDTO ;
import br.com.exemplo.vendas.util.exception.LayerException;
import br.com.exemplo.vendas.util.locator.ServiceLocator ;
import br.com.exemplo.vendas.util.locator.ServiceLocatorFactory ;

public class TesterEJB
{

	public static void main( String[ ] args ) throws Exception
	{
		Hashtable prop = new Hashtable( ) ;
		prop.put( InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory" ) ;
		prop.put( InitialContext.PROVIDER_URL, "jnp://localhost:1099" ) ;

		Context ctx = new InitialContext( prop ) ;

		testaUsuario(ctx);
		testaCliente(ctx);
		testaReserva(ctx);
		testaCompras(ctx);
		testProduto(ctx);
		testListCliente(ctx);
		testListCompra(ctx);
	}
	
	private static void testaCliente(Context ctx) throws NamingException, RemoteException, LayerException{
		
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		
		ClienteInterface remoteCliente = (ClienteInterface) ctx.lookup("ClienteBean/remote");
		
		ClienteFisicoVO vo = new ClienteFisicoVO("vinicius1", "Vinicius", "Rua tal", "000000", "Boa", "55555555", "99999999999");
		
		requestDTO.set("clienteVO", vo);
		responseDTO = remoteCliente.inserirCliente(requestDTO);
		
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;
		
		requestDTO.set("clienteVO", new ClienteJuridicoVO("vinicius2", "Vinicius", "Rua tal", "000000", "Boa", "3333333333", "7777777777"));
		responseDTO = remoteCliente.inserirCliente(requestDTO);
		
		responseDTO = remoteCliente.selecionarTodosClientes( requestDTO ) ;
		ClienteVO[ ] lista = ( ClienteVO[ ] ) responseDTO.get( "listaCliente" ) ;
		if (lista != null)
		{
			for (int i = 0; i < lista.length; i++)
			{
				ClienteVO clienteVO = ( ClienteVO ) lista[ i ] ;
				System.out.println( clienteVO ) ;
			}
		}
		
		responseDTO = remoteCliente.selecionarClientesComCompra( requestDTO ) ;
		ClienteVO[ ] listaComCompra = ( ClienteVO[ ] ) responseDTO.get( "listaCliente" ) ;
		if (listaComCompra != null)
		{
			for (int i = 0; i < listaComCompra.length; i++)
			{
				ClienteVO clienteVO = ( ClienteVO ) listaComCompra[ i ] ;
				System.out.println( clienteVO ) ;
			}
		}
		
		
		
	}

	private static void testaUsuario(Context ctx) throws NamingException,
			LayerException, RemoteException {
		UsuarioInterface remoteUsuario = ( UsuarioInterface ) ctx.lookup( "UsuarioBean/remote" ) ;
		// ProdutoInterface remoteProduto = (ProdutoInterface)
		// ctx.lookup("ProdutoBean/remote");

		ServiceLocator serviceLocator = ServiceLocatorFactory.getServiceLocator( "serviceLocator" ) ;
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		/**
		 * Inserir usuario
		 */
		UsuarioVO vo = new UsuarioVO( "vinicius1", "senha1111", "grupo1111", "perfil1111", "S",
				new Date(System.currentTimeMillis())) ;
		requestDTO.set( "usuarioVO", vo ) ;
		responseDTO = remoteUsuario.inserirUsuario( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;
		
		requestDTO.set( "usuarioVO",  new UsuarioVO( "vinicius2", "senha2222", "grupo2222", "perfil22222", "S",
				new Date(System.currentTimeMillis())) ) ;
		responseDTO = remoteUsuario.inserirUsuario( requestDTO ) ;

		/**
		 * Consultar usuario
		 */

		responseDTO = remoteUsuario.selecionarTodosUsuario( requestDTO ) ;
		UsuarioVO[ ] lista = ( UsuarioVO[ ] ) responseDTO.get( "listaUsuario" ) ;
		if (lista != null)
		{
			for (int i = 0; i < lista.length; i++)
			{
				UsuarioVO usuarioVO = ( UsuarioVO ) lista[ i ] ;
				System.out.println( usuarioVO ) ;
			}
		}
	}
	
	private static void testaReserva(Context ctx) throws NamingException, RemoteException, LayerException{
		ReservaInterface remoteReserva = (ReservaInterface) ctx.lookup("ReservaBean/remote");
		
		//Inserir compra
		ReservaVO vo = new ReservaVO(null, new Date(), "atendente", "C", new BigDecimal(500.00), 1L);
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		requestDTO.set("reservaVO", vo);
		responseDTO = remoteReserva.inserirReserva(requestDTO);
		
		Boolean sucesso = (Boolean) responseDTO.get("resposta");
		
		System.out.println(sucesso);
		
		//Consultar compra
		responseDTO = remoteReserva.selecionarTodosReserva(requestDTO);
		
		ReservaVO[] lista = (ReservaVO[]) responseDTO.get("listaReserva");
		
		if(lista != null){
			for (ReservaVO reservaVO : lista) {
				System.out.println(reservaVO);
			}
		}
		
	}
	
	private static void testaCompras(Context ctx) throws NamingException, RemoteException, LayerException{
		CompraInterface remoteCompra = (CompraInterface) ctx.lookup("CompraBean/remote");
		
		//Inserir compra
		List<CompraVO> listaCompras = new ArrayList<CompraVO>();
		listaCompras.add(new CompraVO(null, new Date(), "Responsavel", "C", new BigDecimal(450.00), null, 1L));
		listaCompras.add(new CompraVO(null, new Date(), "Responsavel", "C", new BigDecimal(500.00), 1L, 1L));
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		for (CompraVO vo : listaCompras) {
			requestDTO.set("compraVO", vo);
			responseDTO = remoteCompra.inserirCompra(requestDTO);
			
			Boolean sucesso = (Boolean) responseDTO.get("resposta");
		}
		//Consultar compra
		responseDTO = remoteCompra.selecionarComprasEntreValores(requestDTO);
		
		CompraVO[] lista = (CompraVO[]) responseDTO.get("listaCompras");
		
		if(lista != null){
			for (CompraVO compraVO : lista) {
				System.out.println(compraVO);
			}
		}
		
	}
	
	private static void testProduto(Context ctx) throws NamingException, RemoteException, LayerException{
		ProdutoInterface remoteProduto = (ProdutoInterface) ctx.lookup("ProdutoBean/remote");
		
		List<ProdutoVO> listaProdutos = new ArrayList<ProdutoVO>();
		
		listaProdutos.add(new ProdutoVO(null, "Livro 1", new BigDecimal(90.00), 2L));
		listaProdutos.add(new ProdutoVO(null, "Livro 2", new BigDecimal(80.00), 1L));
		listaProdutos.add(new ProdutoVO(null, "Livro 3", new BigDecimal(100.00), 3L));
		listaProdutos.add(new ProdutoVO(null, "Computador 1", new BigDecimal(1000.00), 2L));
		listaProdutos.add(new ProdutoVO(null, "Computador 2", new BigDecimal(1500.00), 1L));
		listaProdutos.add(new ProdutoVO(null, "Computador 3", new BigDecimal(900.00), 3L));
		
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		
		for (ProdutoVO vo : listaProdutos) {
			requestDTO.set("produtoVO", vo);
			responseDTO = remoteProduto.inserirProduto(requestDTO);
			Boolean sucesso = (Boolean) responseDTO.get("resposta");
		}
		
		responseDTO = remoteProduto.selecionarProdutosComValorEstoque(requestDTO);
		
		ProdutoVO[] lista = (ProdutoVO[]) responseDTO.get("listaProdutos");
		if(lista!=null){
			for (ProdutoVO produtoVO : lista) {
				System.out.println(produtoVO);
			}
		}
		
	}
	
	private static void testListCliente(Context ctx) throws NamingException, RemoteException, LayerException{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		
		ClienteInterface remoteCliente = (ClienteInterface) ctx.lookup("ClienteBean/remote");
		
		responseDTO = remoteCliente.selecionarClientesComCompra(requestDTO);
		
		ClienteVO[] lista = (ClienteVO[]) responseDTO.get("listaCliente");
		if(lista!=null){
			for (ClienteVO clienteVO : lista) {
				System.out.println(clienteVO);
			}
		}
		
	}
	
	private static void testListCompra(Context ctx) throws NamingException, RemoteException, LayerException{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		
		CompraInterface remoteCompra = (CompraInterface) ctx.lookup("CompraBean/remote");
		
		responseDTO = remoteCompra.selecionarComprasComReserva(requestDTO);
		
		CompraVO[] lista = (CompraVO[]) responseDTO.get("listaCompras");
		if(lista!=null){
			for (CompraVO compraVO : lista) {
				System.out.println(compraVO);
			}
		}
		
	}

}
