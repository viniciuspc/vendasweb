package br.com.exemplo.vendas.negocio.ejb.interfaces;

import javax.ejb.Local;

import br.com.exemplo.vendas.negocio.interfaces.PreparaBaseDadosInterface;

@Local
public interface PreparaBaseDadosLocal extends PreparaBaseDadosInterface {

}
