package br.com.exemplo.vendas.negocio.ejb.interfaces;

import javax.ejb.Remote;

import br.com.exemplo.vendas.negocio.interfaces.PreparaBaseDadosInterface;

@Remote
public interface PreparaBaseDadosRemote extends PreparaBaseDadosInterface {

}
