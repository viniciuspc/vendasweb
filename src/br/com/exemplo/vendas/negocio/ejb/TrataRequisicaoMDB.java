package br.com.exemplo.vendas.negocio.ejb ;

import java.io.Serializable;
import java.util.Hashtable;

import javax.ejb.ActivationConfigProperty ;
import javax.ejb.MessageDriven ;
import javax.jms.Message ;
import javax.jms.MessageListener ;
import javax.jms.ObjectMessage ;
import javax.naming.Context;
import javax.naming.InitialContext;

import br.com.exemplo.vendas.negocio.interfaces.ClienteInterface;
import br.com.exemplo.vendas.negocio.interfaces.UsuarioInterface;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.negocio.model.vo.UsuarioVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;

@MessageDriven( mappedName = "jms/TrataRequisicaoMDB", activationConfig =
{
		@ActivationConfigProperty( propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge" ),

		@ActivationConfigProperty( propertyName = "destinationType", propertyValue = "javax.jms.Queue" ),

		@ActivationConfigProperty( propertyName = "destination", propertyValue = "queue/RecebeRequisicaoQueue" ) } )
public class TrataRequisicaoMDB implements MessageListener
{

	public void onMessage( Message msg )
	{

		try
		{
			Serializable serializable = ( ( ObjectMessage ) msg ).getObject( );
			
			if(serializable instanceof String){
				String vo = ( String ) serializable ;
				
				System.out.println( " recebido " + vo ) ;
			} else {
				
				Hashtable<String, String> prop = new Hashtable<String, String>( ) ;
				prop.put( InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory" ) ;
				prop.put( InitialContext.PROVIDER_URL, "jnp://localhost:1099" ) ;
				
				
				Context ctx = new InitialContext( prop ) ;
				
				if(serializable instanceof UsuarioVO){
					ServiceDTO requestDTO = new ServiceDTO();
					ServiceDTO responseDTO = new ServiceDTO();
					
					UsuarioInterface remoteUsuario = ( UsuarioInterface ) ctx.lookup( "UsuarioBean/remote" ) ;
					
					requestDTO.set("usuarioVO", serializable);
					
					responseDTO = remoteUsuario.inserirUsuario(requestDTO);
					
					Boolean resposta = (Boolean) responseDTO.get("resposta");
					
					msg.setBooleanProperty("resposta", resposta);
					
					
				} else if(serializable instanceof ClienteVO){
					ServiceDTO requestDTO = new ServiceDTO();
					ServiceDTO responseDTO = new ServiceDTO();
					
					ClienteInterface remoteCliente = (ClienteInterface) ctx.lookup("ClienteBean/remote");
					
					requestDTO.set("clienteVO", serializable);
					
					responseDTO = remoteCliente.inserirCliente(requestDTO);
					
					Boolean resposta = (Boolean) responseDTO.get("resposta");
					
					msg.setBooleanProperty("resposta", resposta);
					
					
				}
			
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}