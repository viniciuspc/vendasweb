package br.com.exemplo.vendas.negocio.ejb;

import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.exemplo.vendas.negocio.dao.DaoFactory;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ItemLocal;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ItemRemote;
import br.com.exemplo.vendas.negocio.entity.Compra;
import br.com.exemplo.vendas.negocio.entity.Item;
import br.com.exemplo.vendas.negocio.entity.Produto;
import br.com.exemplo.vendas.negocio.entity.Reserva;
import br.com.exemplo.vendas.negocio.model.vo.ItemVO;
import br.com.exemplo.vendas.util.ConverterEntityVO;
import br.com.exemplo.vendas.util.ConverterVOEntity;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

@Stateless
public class ItemBean implements ItemLocal, ItemRemote  {
	
	@PersistenceContext( unitName = "Vendas" )
	EntityManager em ;

	@Override
	public ServiceDTO inserirItem(ServiceDTO requestDTO) throws LayerException,
			RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		ItemVO itemVO = ( ItemVO ) requestDTO.get( "itemVO" ) ;
		if (itemVO != null)
		{
			Compra compra = DaoFactory.getCompraDAO(em).localizar(itemVO.getCompraNumero());
			Reserva reserva = DaoFactory.getReservaDAO(em).localizar(itemVO.getReservaCodigo());
			Produto produto = DaoFactory.getProdutoDAO(em).localizar(itemVO.getProdutoCodigo());
			Item item = ConverterVOEntity.getInstancia().converteItemVOEntity(itemVO, compra, reserva, produto);
			if (DaoFactory.getItemDAO(em).inserir(item))
			{
				responseDTO.set( "resposta", new Boolean( true ) ) ;
			}
			else
			{
				responseDTO.set( "resposta", new Boolean( false ) ) ;
			}
		}
		return responseDTO ;
	}

	@Override
	public ServiceDTO alterarItem(ServiceDTO requestDTO) throws LayerException,
			RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		ItemVO itemVO = ( ItemVO ) requestDTO.get( "itemVO" ) ;
		if (itemVO != null)
		{
			Compra compra = DaoFactory.getCompraDAO(em).localizar(itemVO.getCompraNumero());
			Reserva reserva = DaoFactory.getReservaDAO(em).localizar(itemVO.getReservaCodigo());
			Produto produto = DaoFactory.getProdutoDAO(em).localizar(itemVO.getProdutoCodigo());
			Item item = ConverterVOEntity.getInstancia().converteItemVOEntity(itemVO, compra, reserva, produto);
			if (DaoFactory.getItemDAO(em).alterar(item))
			{
				responseDTO.set( "resposta", new Boolean( true ) ) ;
			}
			else
			{
				responseDTO.set( "resposta", new Boolean( false ) ) ;
			}
		}
		return responseDTO ;
	}

	@Override
	public ServiceDTO excluirItem(ServiceDTO requestDTO) throws LayerException,
			RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		ItemVO itemVO = ( ItemVO ) requestDTO.get( "itemVO" ) ;
		if (itemVO != null)
		{
			Item item = new Item();
			item.setId(itemVO.getCompraNumero());
			if (DaoFactory.getItemDAO( em ).excluir( item ))
			{
				responseDTO.set( "resposta", new Boolean( true ) ) ;
			}
			else
			{
				responseDTO.set( "resposta", new Boolean( false ) ) ;
			}
		}
		return responseDTO ;
	}

	@Override
	public ServiceDTO selecionarTodosItem(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		Item item = null;
		List<Item> lista = DaoFactory.getItemDAO(em).listar();
		if ((lista != null) && (!lista.isEmpty())) {
			ItemVO[] itens = new ItemVO[lista.size()];
			for (int i = 0; i < lista.size(); i++) {
				item = (Item) lista.get(i);
				ItemVO itemVO = ConverterEntityVO.getInstancia()
						.converteItemEntityVO(item);

				itens[i] = itemVO;
			}
			responseDTO.set("listaItem", itens);
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO getItem(ServiceDTO requestDTO, String login)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		ItemVO itemVO = (ItemVO) requestDTO.get("itemVO");
		if(itemVO != null){
			Item item = DaoFactory.getItemDAO(em).localizar(itemVO.getId());
			if(item != null){
				ItemVO getItem = ConverterEntityVO.getInstancia().converteItemEntityVO(item);
				responseDTO.set("getItem", getItem);
			}
		}
		return responseDTO;
		
	}

}
