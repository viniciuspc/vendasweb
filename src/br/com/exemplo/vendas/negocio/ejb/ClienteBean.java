package br.com.exemplo.vendas.negocio.ejb;

import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.exemplo.vendas.negocio.dao.DaoFactory;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ClienteLocal;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ClienteRemote;
import br.com.exemplo.vendas.negocio.entity.Cliente;
import br.com.exemplo.vendas.negocio.entity.Usuario;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.util.ConverterEntityVO;
import br.com.exemplo.vendas.util.ConverterVOEntity;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

@Stateless
public class ClienteBean implements ClienteLocal, ClienteRemote {

	@PersistenceContext(unitName = "Vendas")
	EntityManager em;

	@Override
	public ServiceDTO inserirCliente(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		ClienteVO vo = (ClienteVO) requestDTO.get("clienteVO");
		if (vo != null) {
			Usuario usuario = new Usuario();
			usuario.setLogin(vo.getUsuarioLogin());
			Usuario usuarioBanco = DaoFactory.getUsuarioDAO(em)
					.localizarPorLogin(usuario);
			Cliente cliente;

			cliente = ConverterVOEntity.getInstancia().converteClienteVOEntity(vo, usuarioBanco);

			if (DaoFactory.getClienteDAO(em).inserir(cliente)) {
				responseDTO.set("resposta", new Boolean(true));
			} else {
				responseDTO.set("resposta", new Boolean(false));
			}
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO selecionarTodosClientes(ServiceDTO requestDTO)
			throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		Cliente cliente = null;
		List<Cliente> lista = DaoFactory.getClienteDAO(em).listar();
		if ((lista != null) && (!lista.isEmpty())) {
			ClienteVO[] clientes = new ClienteVO[lista.size()];
			for (int i = 0; i < lista.size(); i++) {
				cliente = (Cliente) lista.get(i);
				ClienteVO clienteVO;

				clienteVO = ConverterEntityVO.getInstancia()
						.converteClienteEtityVo(cliente);

				clientes[i] = clienteVO;
			}
			responseDTO.set("listaCliente", clientes);
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO selecionarClientesComCompra(ServiceDTO requestDTO)
			throws LayerException, RemoteException {

		ServiceDTO responseDTO = new ServiceDTO();
		Cliente cliente = null;
		List<Cliente> lista = DaoFactory.getClienteDAO(em).listarComCompras();
		if ((lista != null) && (!lista.isEmpty())) {
			ClienteVO[] clientes = new ClienteVO[lista.size()];
			for (int i = 0; i < lista.size(); i++) {
				cliente = (Cliente) lista.get(i);
				ClienteVO clienteVO;

				clienteVO = ConverterEntityVO.getInstancia()
						.converteClienteEtityVo(cliente);

				clientes[i] = clienteVO;
			}
			responseDTO.set("listaCliente", clientes);
		}
		return responseDTO;

	}

	public ServiceDTO alterarCliente(ServiceDTO requestDTO)
			throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		ClienteVO vo = (ClienteVO) requestDTO.get("clienteVO");
		if (vo != null) {
			Usuario usuario = new Usuario();
			usuario.setLogin(vo.getUsuarioLogin());
			Usuario usuarioBanco = DaoFactory.getUsuarioDAO(em)
					.localizarPorLogin(usuario);
			Cliente cliente;

			cliente = ConverterVOEntity.getInstancia().converteClienteVOEntity(vo, usuarioBanco);

			if (DaoFactory.getClienteDAO(em).alterar(cliente)) {
				responseDTO.set("resposta", new Boolean(true));
			} else {
				responseDTO.set("resposta", new Boolean(false));
			}
		}
		return responseDTO;
	}

	public ServiceDTO excluirCliente(ServiceDTO requestDTO)
			throws LayerException {
		ServiceDTO responseDTO = new ServiceDTO();
		ClienteVO clienteVO = (ClienteVO) requestDTO.get("clienteVO");
		if (clienteVO != null) {
			Usuario usuario = new Usuario();
			usuario.setLogin(clienteVO.getUsuarioLogin());
			if (DaoFactory.getUsuarioDAO(em).excluir(usuario)) {
				responseDTO.set("resposta", new Boolean(true));
			} else {
				responseDTO.set("resposta", new Boolean(false));
			}
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO getCliente(ServiceDTO requestDTO) throws LayerException,
			RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		ClienteVO clienteVO = (ClienteVO) requestDTO.get("clienteVO");
		if(clienteVO != null){
			Cliente cliente = DaoFactory.getClienteDAO(em).localizar(clienteVO.getCodigo());
			if(cliente != null){
				ClienteVO getCliente = ConverterEntityVO.getInstancia().converteClienteEtityVo(cliente);
				responseDTO.set("getCliente", getCliente);
			}
		}
		
		return responseDTO;
		
	}
	
	

}
