package br.com.exemplo.vendas.negocio.ejb;

import java.rmi.RemoteException;

import javax.ejb.Stateless;

import br.com.exemplo.vendas.negocio.ejb.interfaces.PreparaBaseDadosLocal;
import br.com.exemplo.vendas.negocio.ejb.interfaces.PreparaBaseDadosRemote;
import br.com.exemplo.vendas.util.exception.LayerException;

@Stateless
public class PreparaBaseDadosBean implements PreparaBaseDadosLocal,PreparaBaseDadosRemote {

	@Override
	public void popularBaseDados() throws LayerException, RemoteException {
		PreparaBaseDadosThread thread = new PreparaBaseDadosThread();
		new Thread(thread).start();
		
	}


}
