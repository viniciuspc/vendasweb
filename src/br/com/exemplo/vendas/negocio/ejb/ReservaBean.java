package br.com.exemplo.vendas.negocio.ejb;

import java.rmi.RemoteException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.exemplo.vendas.negocio.dao.DaoFactory;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ReservaLocal;
import br.com.exemplo.vendas.negocio.ejb.interfaces.ReservaRemote;
import br.com.exemplo.vendas.negocio.entity.Cliente;
import br.com.exemplo.vendas.negocio.entity.Reserva;
import br.com.exemplo.vendas.negocio.model.vo.ReservaVO;
import br.com.exemplo.vendas.util.ConverterEntityVO;
import br.com.exemplo.vendas.util.ConverterVOEntity;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

@Stateless
public class ReservaBean implements ReservaLocal,ReservaRemote  {
	
	@PersistenceContext( unitName = "Vendas" )
	EntityManager em ;

	@Override
	public ServiceDTO inserirReserva(ServiceDTO requestDTO) throws LayerException,
			RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		ReservaVO reservaVO = ( ReservaVO ) requestDTO.get( "reservaVO" ) ;
		if (reservaVO != null)
		{
			Cliente cliente = DaoFactory.getClienteDAO(em).localizar(reservaVO.getClienteCodigo());
			Reserva reserva = ConverterVOEntity.getInstancia().converteReservaVOEntity(reservaVO, cliente);
			if (DaoFactory.getReservaDAO(em).inserir(reserva))
			{
				responseDTO.set( "resposta", new Boolean( true ) ) ;
			}
			else
			{
				responseDTO.set( "resposta", new Boolean( false ) ) ;
			}
		}
		return responseDTO ;
	}

	@Override
	public ServiceDTO alterarReserva(ServiceDTO requestDTO) throws LayerException,
			RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		ReservaVO reservaVO = ( ReservaVO ) requestDTO.get( "reservaVO" ) ;
		if (reservaVO != null)
		{
			Cliente cliente = DaoFactory.getClienteDAO(em).localizar(reservaVO.getClienteCodigo());
			Reserva reserva = ConverterVOEntity.getInstancia().converteReservaVOEntity(reservaVO, cliente);
			if (DaoFactory.getReservaDAO(em).alterar(reserva))
			{
				responseDTO.set( "resposta", new Boolean( true ) ) ;
			}
			else
			{
				responseDTO.set( "resposta", new Boolean( false ) ) ;
			}
		}
		return responseDTO ;
	}

	@Override
	public ServiceDTO excluirReserva(ServiceDTO requestDTO) throws LayerException,
			RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		ReservaVO reservaVO = ( ReservaVO ) requestDTO.get( "reservaVO" ) ;
		if (reservaVO != null)
		{
			Reserva reserva = new Reserva();
			reserva.setCodigo(reservaVO.getCodigo());
			if (DaoFactory.getReservaDAO( em ).excluir( reserva ))
			{
				responseDTO.set( "resposta", new Boolean( true ) ) ;
			}
			else
			{
				responseDTO.set( "resposta", new Boolean( false ) ) ;
			}
		}
		return responseDTO ;
	}

	@Override
	public ServiceDTO selecionarTodosReserva(ServiceDTO requestDTO)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO();
		Reserva reserva = null;
		List<Reserva> lista = DaoFactory.getReservaDAO(em).listar();
		if ((lista != null) && (!lista.isEmpty())) {
			ReservaVO[] reservas = new ReservaVO[lista.size()];
			for (int i = 0; i < lista.size(); i++) {
				reserva = (Reserva) lista.get(i);
				ReservaVO reservaVO = ConverterEntityVO.getInstancia()
						.converteReservaEntityVO(reserva);

				reservas[i] = reservaVO;
			}
			responseDTO.set("listaReserva", reservas);
		}
		return responseDTO;
	}

	@Override
	public ServiceDTO getReserva(ServiceDTO requestDTO, String login)
			throws LayerException, RemoteException {
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		ReservaVO reservaVO = (ReservaVO) requestDTO.get("reservaVO");
		if(reservaVO != null){
			Reserva reserva = DaoFactory.getReservaDAO(em).localizar(reservaVO.getCodigo());
			if(reserva != null){
				ReservaVO getReserva = ConverterEntityVO.getInstancia().converteReservaEntityVO(reserva);
				responseDTO.set("getReserva", getReserva);
			}
		}
		return responseDTO;
		
	}

}
