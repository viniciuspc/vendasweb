package br.com.exemplo.vendas.negocio.ejb;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import br.com.exemplo.vendas.negocio.interfaces.ClienteInterface;
import br.com.exemplo.vendas.negocio.interfaces.CompraInterface;
import br.com.exemplo.vendas.negocio.interfaces.ProdutoInterface;
import br.com.exemplo.vendas.negocio.interfaces.ReservaInterface;
import br.com.exemplo.vendas.negocio.interfaces.UsuarioInterface;
import br.com.exemplo.vendas.negocio.model.vo.ClienteFisicoVO;
import br.com.exemplo.vendas.negocio.model.vo.ClienteJuridicoVO;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.negocio.model.vo.ReservaVO;
import br.com.exemplo.vendas.negocio.model.vo.UsuarioVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

public class PreparaBaseDadosThread implements Runnable {
	
	
	@Override
	public void run() {
		Hashtable<String, String> prop = new Hashtable<String, String>( ) ;
		prop.put( InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory" ) ;
		prop.put( InitialContext.PROVIDER_URL, "jnp://localhost:1099" ) ;
		
		try {
			Context ctx = new InitialContext( prop ) ;
			System.out.println("Criando usu�rio 1/5");
			criarUsuario(ctx);
			System.out.println("Criando cliete 2/5");
			criarCliente(ctx);
			System.out.println("Criando reserva 3/5");
			criarReserva(ctx);
			System.out.println("Criando compra 4/5");
			criarCompras(ctx);
			System.out.println("Criando produto 5/5");
			criarProduto(ctx);
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (LayerException e) {
			e.printStackTrace();
		}
		
	}
	
	private void criarCliente(Context ctx) throws NamingException, RemoteException, LayerException{
		
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		
		ClienteInterface remoteCliente = (ClienteInterface) ctx.lookup("ClienteBean/remote");
		
		ClienteFisicoVO vo = new ClienteFisicoVO("vinicius1", "Vinicius", "Rua tal", "000000", "Boa", "55555555", "99999999999");
		
		requestDTO.set("clienteVO", vo);
		responseDTO = remoteCliente.inserirCliente(requestDTO);
		
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;
		
		requestDTO.set("clienteVO", new ClienteJuridicoVO("vinicius2", "Vinicius", "Rua tal", "000000", "Boa", "3333333333", "7777777777"));
		responseDTO = remoteCliente.inserirCliente(requestDTO);
	}

	private void criarUsuario(Context ctx) throws NamingException,
			LayerException, RemoteException {
		UsuarioInterface remoteUsuario = ( UsuarioInterface ) ctx.lookup( "UsuarioBean/remote" ) ;
		
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		/**
		 * Inserir usuario
		 */
		UsuarioVO vo = new UsuarioVO( "vinicius1", "senha1111", "grupo1111", "perfil1111", "S",
				new Date(System.currentTimeMillis())) ;
		requestDTO.set( "usuarioVO", vo ) ;
		responseDTO = remoteUsuario.inserirUsuario( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;
		
		requestDTO.set( "usuarioVO",  new UsuarioVO( "vinicius2", "senha2222", "grupo2222", "perfil22222", "S",
				new Date(System.currentTimeMillis())) ) ;
		responseDTO = remoteUsuario.inserirUsuario( requestDTO ) ;
	}
	
	private void criarReserva(Context ctx) throws NamingException, RemoteException, LayerException{
		ReservaInterface remoteReserva = (ReservaInterface) ctx.lookup("ReservaBean/remote");
		
		//Inserir compra
		ReservaVO vo = new ReservaVO(null, new Date(), "atendente", "C", new BigDecimal(500.00), 1L);
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		requestDTO.set("reservaVO", vo);
		responseDTO = remoteReserva.inserirReserva(requestDTO);
		
		Boolean sucesso = (Boolean) responseDTO.get("resposta");
		
	}
	
	private void criarCompras(Context ctx) throws NamingException, RemoteException, LayerException{
		CompraInterface remoteCompra = (CompraInterface) ctx.lookup("CompraBean/remote");
		
		//Inserir compra
		List<CompraVO> listaCompras = new ArrayList<CompraVO>();
		listaCompras.add(new CompraVO(null, new Date(), "Responsavel", "C", new BigDecimal(450.00), null, 1L));
		listaCompras.add(new CompraVO(null, new Date(), "Responsavel", "C", new BigDecimal(500.00), 1L, 1L));
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		for (CompraVO vo : listaCompras) {
			requestDTO.set("compraVO", vo);
			responseDTO = remoteCompra.inserirCompra(requestDTO);
			
			Boolean sucesso = (Boolean) responseDTO.get("resposta");
		}
		
	}
	
	private void criarProduto(Context ctx) throws NamingException, RemoteException, LayerException{
		ProdutoInterface remoteProduto = (ProdutoInterface) ctx.lookup("ProdutoBean/remote");
		
		List<ProdutoVO> listaProdutos = new ArrayList<ProdutoVO>();
		
		listaProdutos.add(new ProdutoVO(null, "Livro 1", new BigDecimal(90.00), 2L));
		listaProdutos.add(new ProdutoVO(null, "Livro 2", new BigDecimal(80.00), 1L));
		listaProdutos.add(new ProdutoVO(null, "Livro 3", new BigDecimal(100.00), 3L));
		listaProdutos.add(new ProdutoVO(null, "Computador 1", new BigDecimal(1000.00), 2L));
		listaProdutos.add(new ProdutoVO(null, "Computador 2", new BigDecimal(1500.00), 1L));
		listaProdutos.add(new ProdutoVO(null, "Computador 3", new BigDecimal(900.00), 3L));
		
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		
		for (ProdutoVO vo : listaProdutos) {
			requestDTO.set("produtoVO", vo);
			responseDTO = remoteProduto.inserirProduto(requestDTO);
			Boolean sucesso = (Boolean) responseDTO.get("resposta");
		}
	}

}
