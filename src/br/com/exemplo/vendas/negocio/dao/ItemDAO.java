package br.com.exemplo.vendas.negocio.dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.exemplo.vendas.negocio.entity.Item;

public class ItemDAO extends GenericDAO<Item> {

	public ItemDAO(EntityManager em) {
		super(em);
	}

	public ItemDAO() {
		super(Persistence.createEntityManagerFactory("Vendas")
				.createEntityManager());
	}

	public boolean alterar(Item item) {
		boolean result = false;
		Item existenteItem = null;

		try {
			existenteItem = em.find(Item.class, item.getId());
			if (existenteItem != null) {
				em.merge(item);
				result = true;
			} else {
				result = false;
			}
		} catch (Exception e) {
			if (debugInfo) {
				e.printStackTrace();
			}
			result = false;
		}
		return result;
	}

	public boolean excluir(Item item) {
		Item obj = null;
		boolean result = false;

		try {
			Query q = em.createQuery("from Item where id = :id");
			q.setParameter("id", item.getId());
			obj = (Item) q.getSingleResult();
			em.remove(obj);
			result = true;
		} catch (Exception e) {
			if (debugInfo) {
				e.printStackTrace();
			}
		}
		return result;
	}

}
