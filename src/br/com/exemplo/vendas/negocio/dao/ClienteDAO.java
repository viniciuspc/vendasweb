package br.com.exemplo.vendas.negocio.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.exemplo.vendas.negocio.entity.Cliente;
import br.com.exemplo.vendas.negocio.entity.Usuario;

public class ClienteDAO extends GenericDAO<Cliente> {

	public ClienteDAO(EntityManager em) {
		super(em);
	}

	public ClienteDAO() {
		super(Persistence.createEntityManagerFactory("Vendas")
				.createEntityManager());
	}

	public boolean inserir(Cliente cliente) {
		boolean result = false;

		try {
			em.persist(cliente);
			result = true;
		} catch (Exception e) {
			if (debugInfo) {
				e.printStackTrace();
			}
		}

		return result;
	}

	public List<Cliente> listarComCompras() {
		List<Cliente> list = null;

		try {

			Query query = em.createQuery("select distinct(c.cliente) from Compra c");
			list = (List<Cliente>) query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
	
	public boolean alterar( Cliente cliente )
	{
		boolean result = false ;
		Cliente existenteCliente = null ;

		try
		{
			existenteCliente = em.find( Cliente.class, cliente.getCodigo()) ;
			if (existenteCliente != null)
			{
				em.merge( cliente ) ;
				result = true ;
			}
			else
			{
				result = false ;
			}
		}
		catch (Exception e)
		{
			if (debugInfo)
			{
				e.printStackTrace( ) ;
			}
			result = false ;
		}
		return result ;
	}

	public boolean excluir( Cliente cliente )
	{
		Cliente obj = null ;
		boolean result = false ;

		try
		{
			Query q = em.createQuery( "from Cliente where codigo = :codigo" ) ;
			q.setParameter( "codigo", cliente.getCodigo() ) ;
			obj = ( Cliente ) q.getSingleResult( ) ;
			em.remove( obj ) ;
			result = true ;
		}
		catch (Exception e)
		{
			if (debugInfo)
			{
				e.printStackTrace( ) ;
			}
		}
		return result ;
	}

}
