package br.com.exemplo.vendas.negocio.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.exemplo.vendas.negocio.entity.Produto;

public class ProdutoDAO extends GenericDAO<Produto> {

	public ProdutoDAO(EntityManager em) {
		super(em);
	}
	
	public ProdutoDAO(){
		super(Persistence.createEntityManagerFactory("Vendas")
				.createEntityManager());
	}

	public boolean inserir(Produto produto) {
		boolean result = false;

		try {
			em.persist(produto);
			result = true;
		} catch (Exception e) {
			if (debugInfo) {
				e.printStackTrace();
			}
		}

		return result;
	}

	public List<Produto> listarComValorEstoque(BigDecimal valorLimite, Long quantidadeLimite) {
		List<Produto> list = null;

		try {

			Query query = em.createQuery("from Produto p where p.preco < :valorLimite AND p.estoque >= :quantidadeLimite");
			query.setParameter("valorLimite", valorLimite);
			query.setParameter("quantidadeLimite", quantidadeLimite);
			list = (List<Produto>) query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}
	
	public boolean alterar( Produto produto )
	{
		boolean result = false ;
		Produto existenteProduto = null ;

		try
		{
			existenteProduto = em.find( Produto.class, produto.getCodigo()) ;
			if (existenteProduto != null)
			{
				em.merge( produto ) ;
				result = true ;
			}
			else
			{
				result = false ;
			}
		}
		catch (Exception e)
		{
			if (debugInfo)
			{
				e.printStackTrace( ) ;
			}
			result = false ;
		}
		return result ;
	}

	public boolean excluir( Produto produto )
	{
		Produto obj = null ;
		boolean result = false ;

		try
		{
			Query q = em.createQuery( "from Compra where numero = :numero" ) ;
			q.setParameter( "numero", produto.getCodigo() ) ;
			obj = ( Produto ) q.getSingleResult( ) ;
			em.remove( obj ) ;
			result = true ;
		}
		catch (Exception e)
		{
			if (debugInfo)
			{
				e.printStackTrace( ) ;
			}
		}
		return result ;
	}
	
	

}
