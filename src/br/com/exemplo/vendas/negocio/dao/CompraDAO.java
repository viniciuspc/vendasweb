package br.com.exemplo.vendas.negocio.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.com.exemplo.vendas.negocio.entity.Compra;

public class CompraDAO extends GenericDAO<Compra> {

	public CompraDAO(EntityManager em) {
		super(em);
	}

	public CompraDAO() {
		super(Persistence.createEntityManagerFactory("Vendas")
				.createEntityManager());
	}

	public boolean inserir(Compra compra) {
		boolean result = false;

		try {
			em.persist(compra);
			result = true;
		} catch (Exception e) {
			//if (debugInfo) {
				e.printStackTrace();
			//}
		}

		return result;
	}

	public List<Compra> listarComValor(BigDecimal valorMinimo,
			BigDecimal valorMaximo) {
		List<Compra> list = null;

		try {

			Query query = em.createQuery("from Compra c where c.valor > :valorMinimo AND c.valor < :valorMaximo");
			query.setParameter("valorMinimo", valorMinimo);
			query.setParameter("valorMaximo", valorMaximo);
			list = (List<Compra>) query.getResultList();

		} catch (Exception e) {
		}

		return list;
	}

	public List<Compra> listarComReserva() {
		List<Compra> list = null;

		try {

			Query query = em.createQuery("from Compra c where c.reserva is not null");
			list = (List<Compra>) query.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	public boolean alterar(Compra compra) {
		boolean result = false;
		Compra existentecompra = null;

		try {
			existentecompra = em.find(Compra.class, compra.getNumero());
			if (existentecompra != null) {
				em.merge(compra);
				result = true;
			} else {
				result = false;
			}
		} catch (Exception e) {
			if (debugInfo) {
				e.printStackTrace();
			}
			result = false;
		}
		return result;
	}

	public boolean excluir(Compra compra) {
		Compra obj = null;
		boolean result = false;

		try {
			Query q = em.createQuery("from Compra where numero = :numero");
			q.setParameter("numero", compra.getNumero());
			obj = (Compra) q.getSingleResult();
			em.remove(obj);
			result = true;
		} catch (Exception e) {
			if (debugInfo) {
				e.printStackTrace();
			}
		}
		return result;
	}

}
