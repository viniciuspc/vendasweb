package br.com.exemplo.vendas.apresentacao.actions;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

import br.com.exemplo.vendas.apresentacao.service.Service;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.util.exception.LayerException;

@ManagedBean(name = "listaC")
@RequestScoped
public class ListarBean {
	
	
	Service service = new Service();
	//ServiceOff service = new ServiceOff();
	
	private DataModel listaCompra;
	private DataModel listaCliente;
	private DataModel listaReserva;
	private DataModel listaProduto;
	private String mensagem;
	
	public String getMensagem() {
		return mensagem;
	}


	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}


	public DataModel getListaCompras() {
		List<CompraVO> lista;
		try {
			lista = service.listarComprasEntreValoresWeb();
			listaCompra = new ListDataModel(lista);		
		} catch (LayerException e) {
			e.printStackTrace();
		}						
		return listaCompra;
	}
	
	
	public DataModel getListaClientes(){
		List<ClienteVO>  lista;
		try {
			lista = service.listarClientesComCompraWeb();
			this.listaCliente = new ListDataModel(lista);
		} catch (LayerException e) {
			e.printStackTrace();
		}			
		return listaCliente;
	}

	public DataModel getListaProduto(){
		List<ProdutoVO>  lista;
		try {
			lista = service.listarProdutosComValorEstoqueWeb();
			this.listaProduto = new ListDataModel(lista);
		} catch (LayerException e) {
			e.printStackTrace();
		}			
		return listaProduto;
	}


	public DataModel getListaReserva(){
		List<CompraVO>  lista;
		try {
			lista = service.listarComprasComReservaWeb();
			this.listaReserva = new ListDataModel(lista);
		} catch (LayerException e) {
			e.printStackTrace();
		}			
		return listaReserva;
	}

	
	public void popularDados(ActionEvent actionEvent) {	
		
		try {
			service.populaBaseDados();
			this.setMensagem("Base foi populada com sucesso!!!");
			
		} catch (LayerException e) {
			e.printStackTrace();
			this.setMensagem("Erro ao execuar o metodo service.popularBaseDados");
		}
		
		
	}


}
