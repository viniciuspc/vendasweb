package br.com.exemplo.vendas.apresentacao.actions;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.context.RequestContext;

import br.com.exemplo.vendas.apresentacao.service.Service;
import br.com.exemplo.vendas.negocio.model.vo.UsuarioVO;
import br.com.exemplo.vendas.util.exception.LayerException;

@ManagedBean(name = "logarBean")
@RequestScoped
public class LogarBean {

	private String username;

	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void login(ActionEvent actionEvent) {
		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage msg = null;
		boolean loggedIn = false;

		if (username != null && username.equals("admin") && password != null
				&& password.equals("admin")) {
			loggedIn = true;
			msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Login Efetuado", username);
		} else {
			loggedIn = false;
			msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Login Inv�lido", "Invalid credentials");
		}

		FacesContext.getCurrentInstance().addMessage(null, msg);
		context.addCallbackParam("loggedIn", loggedIn);
	}

	public String login() throws LayerException {

		FacesMessage msg = null;
		boolean encontrou = false;
		String retorno = null;

		if (username != null && username.equals("admin") && password != null
				&& password.equals("admin")) {

			msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
					"Login Efetuado", username);
			retorno = "menu";

		} else {

			Service service = new Service();
			//ServiceOff service = new ServiceOff();
			List<UsuarioVO> lista = service.listarUsuarios();

			for (UsuarioVO usuarioVO : lista) {
				if (usuarioVO.getLogin().equals(username)
						&& usuarioVO.getSenha().equals(password)) {
					encontrou = true;
				}
				
			}

			if (encontrou == false) {
				msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
						"Login Invalido", "Invalid credentials");
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(username, msg);
				retorno = "index";

			} else {
				msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Login Efetuado", username);
				retorno = "menu";
			}
		}
		
		return retorno;
	}

}
