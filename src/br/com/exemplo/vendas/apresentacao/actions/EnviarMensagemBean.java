package br.com.exemplo.vendas.apresentacao.actions;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


import br.com.exemplo.vendas.apresentacao.service.Service;
import br.com.exemplo.vendas.util.exception.BusinessException;
import br.com.exemplo.vendas.util.exception.LayerException;
import br.com.exemplo.vendas.util.exception.SysException;

@ManagedBean(name="enviarMensagemBean")
@RequestScoped
public class EnviarMensagemBean {
	private String mensagem;
	private String ticket;
	
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public String getTicket() {
		return ticket;
	}
	
	public String enviarMensagem(){
		
		Service service = new Service();
		
		try {
			this.ticket = service.enviaMensagem(this.mensagem);
		} catch (LayerException e) {
			e.printStackTrace();
			if(e instanceof BusinessException){
				//redirecionar para uma p�gina
			} else if(e instanceof SysException){
				//redirecionar para uma p�gina
			}
			
		}
		
		return "/faces/popup";
	}
		
	
}
