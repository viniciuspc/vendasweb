package br.com.exemplo.vendas.apresentacao.service ;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.exemplo.vendas.apresentacao.delegate.BusinessDelegate;
import br.com.exemplo.vendas.negocio.model.vo.ClienteFisicoVO;
import br.com.exemplo.vendas.negocio.model.vo.ClienteJuridicoVO;
import br.com.exemplo.vendas.negocio.model.vo.ClienteVO;
import br.com.exemplo.vendas.negocio.model.vo.CompraVO;
import br.com.exemplo.vendas.negocio.model.vo.ItemVO;
import br.com.exemplo.vendas.negocio.model.vo.ProdutoVO;
import br.com.exemplo.vendas.negocio.model.vo.ReservaVO;
import br.com.exemplo.vendas.negocio.model.vo.UsuarioVO;
import br.com.exemplo.vendas.util.dto.ServiceDTO;
import br.com.exemplo.vendas.util.exception.LayerException;

public class Service
{
	
	/**
	 * WebService
	 * Lista todas as compras com os valores maior que 0 e menor 500
	 * @return
	 * @throws LayerException
	 */
	public CompraVO[ ] listarComprasEntreValores( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarComprasEntreValores(requestDTO);
		CompraVO[ ] compras = ( CompraVO[ ] ) responseDTO.get( "listaCompras" ) ;
		return compras ;
	}
	

	
	/**
	 * Web
	 * Lista todas as compras com os valores maior que 0 e menor 500
	 * @return
	 * @throws LayerException
	 */
	public List<CompraVO> listarComprasEntreValoresWeb( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarComprasEntreValores(requestDTO);
		CompraVO[ ] compras = ( CompraVO[ ] ) responseDTO.get( "listaCompras" ) ;
		
		List<CompraVO> lista = new ArrayList<CompraVO>();
		if(compras != null){
			lista = Arrays.asList( compras ) ;
		}
		
		return lista;
	}

	/**
	 * WebService
	 * Lista os produtos que custam menos que 1000 e que tenham pelo menos 2 unidades em estoque
	 * @return
	 * @throws LayerException
	 */
	public ProdutoVO[ ]listarProdutosComValorEstoque( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarProdutosComValorEstoque(requestDTO);
		ProdutoVO[ ] produtos = ( ProdutoVO[ ] ) responseDTO.get( "listaProdutos" ) ;
		return produtos;
	}

	/**
	 * Web
	 * Lista os produtos que custam menos que 1000 e que tenham pelo menos 2 unidades em estoque
	 * @return
	 * @throws LayerException
	 */
	public List<ProdutoVO> listarProdutosComValorEstoqueWeb( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarProdutosComValorEstoque(requestDTO);
		ProdutoVO[ ] produtos = ( ProdutoVO[ ] ) responseDTO.get( "listaProdutos" ) ;
		
		List<ProdutoVO> lista = new ArrayList<ProdutoVO>();
		if(produtos != null){
			lista = Arrays.asList( produtos ) ;
		}
		
		return lista;	
	}
		
	
	/**
	 * WebService
	 * Lista clientes que fizerem ao menos uma compra
	 * @return
	 * @throws LayerException
	 */
	public ClienteVO[] listarClientesComCompra( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarClientesComCompra(requestDTO);
		ClienteVO[ ] clientes = ( ClienteVO[ ] ) responseDTO.get( "listaCliente" ) ;
		return clientes;
	}
	
	/**
	 * Web
	 * Lista clientes que fizerem ao menos uma compra
	 * @return
	 * @throws LayerException
	 */
	public List<ClienteVO> listarClientesComCompraWeb( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarClientesComCompra(requestDTO);
		ClienteVO[ ] clientes = ( ClienteVO[ ] ) responseDTO.get( "listaCliente" ) ;

		List<ClienteVO> lista = new ArrayList<ClienteVO>();
		if(clientes != null){
			lista = Arrays.asList( clientes ) ;
		}
		
		return lista;
		
	}


	
	/**
	 * WebService
	 * Lista todas as compras que tem reserva
	 * @return
	 * @throws LayerException
	 */
	public CompraVO[ ] listarComprasComReserva( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarComprasComReserva(requestDTO);
		CompraVO[ ] compras = ( CompraVO[ ] ) responseDTO.get( "listaCompras" ) ;
		return compras ;
	}
	

	/**
	 * Web
	 * Lista todas as compras que tem reserva
	 * @return
	 * @throws LayerException
	 */
	public List<CompraVO> listarComprasComReservaWeb( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarComprasComReserva(requestDTO);
		CompraVO[ ] compras = ( CompraVO[ ] ) responseDTO.get( "listaCompras" ) ;
		
		List<CompraVO> lista = new ArrayList<CompraVO>();
		if(compras != null){
			lista = Arrays.asList( compras ) ;
		}
		
		return lista;
		
	
	}

	
	public String enviaMensagem( String mensagem ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		requestDTO.set( "requisicao", mensagem ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirQueue( requestDTO ) ;
		return ( String ) responseDTO.get( "ticket" ) ;
	}
	
	public String inserirUsuarioQueue( UsuarioVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		requestDTO.set( "requisicao", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirQueue( requestDTO ) ;
		return ( String ) responseDTO.get( "ticket" ) ;
	}
	
	public String inserirClienteFisicoQueue( ClienteFisicoVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		requestDTO.set( "requisicao", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirQueue( requestDTO ) ;
		return ( String ) responseDTO.get( "ticket" ) ;
	}
	
	public String inserirClienteJuridicoQueue( ClienteJuridicoVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;
		requestDTO.set( "requisicao", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirQueue( requestDTO ) ;
		return ( String ) responseDTO.get( "ticket" ) ;
	}
	
	public Boolean inserirUsuario( UsuarioVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "usuarioVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirUsuario( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public List<UsuarioVO> listarUsuarios( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selectionarTodosUsuarios( requestDTO ) ;
		UsuarioVO[ ] usuarios = ( UsuarioVO[ ] ) responseDTO.get( "listaUsuario" ) ;
		List<UsuarioVO> lista = new ArrayList<UsuarioVO>();
		if(usuarios != null){
			lista = Arrays.asList( usuarios ) ;
		}
		return lista ;
	}

	public Boolean alterarUsuario( UsuarioVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "usuarioVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).alterarUsuario( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}

	public Boolean excluirUsuario( UsuarioVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "usuarioVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).excluirUsuario( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean inserirClienteFisico( ClienteFisicoVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "clienteVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirCliente( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean inserirClienteJuridico( ClienteJuridicoVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "clienteVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirCliente( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public List<ClienteVO> listarClientes( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarTodosClientes(requestDTO);
		ClienteVO[ ] usuarios = ( ClienteVO[ ] ) responseDTO.get( "listaCliente" ) ;
		List<ClienteVO> lista = new ArrayList<ClienteVO>();
		if(usuarios!=null){
			lista = Arrays.asList( usuarios ) ;
		}
		return lista ;
	}
	
	public Boolean alterarCliente( ClienteVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "clienteVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).alterarCliente( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean excluirCliente( ClienteVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "clienteVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).excluirCliente( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean inserirCompra( CompraVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "comrpaVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirCompra( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public List<CompraVO> listarCompras( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarTodasCompras(requestDTO);
		CompraVO[ ] compras = ( CompraVO[ ] ) responseDTO.get( "listaCompras" ) ;
		List<CompraVO> lista = new ArrayList<CompraVO>();
		if(compras != null){
			lista = Arrays.asList( compras ) ;
		}
		return lista ;
	}
	
	public Boolean alterarCompra( CompraVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "comrpaVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).alterarCompra( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean excluirCompra( CompraVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "comrpaVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).excluirCompra( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean inserirProduto( ProdutoVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "produtoVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirProduto( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public List<ProdutoVO> listarProdutos( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarTodosProdutos(requestDTO);
		ProdutoVO[ ] produtos = ( ProdutoVO[ ] ) responseDTO.get( "listaProdutos" ) ;
		List<ProdutoVO> lista = new ArrayList<ProdutoVO>();
		if(produtos != null){
			lista = Arrays.asList( produtos ) ;
		}
		return lista ;
	}
	
	public Boolean alterarProduto( ProdutoVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "produtoVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).alterarProduto( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean excluriProduto( ProdutoVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "produtoVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).excluirProduto( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean inserirItem( ItemVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "itemVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirItem( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public List<ItemVO> listarItens( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarTodosItem(requestDTO);
		ItemVO[ ] itens = ( ItemVO[ ] ) responseDTO.get( "listaItem" ) ;
		List<ItemVO> lista = new ArrayList<ItemVO>();
		if(itens!=null){
			lista = Arrays.asList( itens ) ;
		}
		return lista ;
	}
	
	public Boolean alterarItem( ItemVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "itemVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).alterarItem( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean excluriItem( ItemVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "itemVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).excluirItem( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean inserirReserva( ReservaVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "reservaVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).inserirReserva( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public List<ReservaVO> listarReservas( ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		responseDTO = BusinessDelegate.getInstance( ).selecionarTodosReserva(requestDTO);
		ReservaVO[ ] itens = ( ReservaVO[ ] ) responseDTO.get( "listaReservas" ) ;
		List<ReservaVO> lista = new ArrayList<ReservaVO>();
		if(itens!=null){
			lista = Arrays.asList( itens ) ;
		}
		return lista ;
	}
	
	public Boolean alterarReserva( ReservaVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "reservaVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).alterarReserva( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	public Boolean excluriReserva( ReservaVO vo ) throws LayerException
	{
		ServiceDTO requestDTO = new ServiceDTO( ) ;
		ServiceDTO responseDTO = new ServiceDTO( ) ;

		requestDTO.set( "reservaVO", vo ) ;
		responseDTO = BusinessDelegate.getInstance( ).excluirReserva( requestDTO ) ;
		Boolean sucesso = ( Boolean ) responseDTO.get( "resposta" ) ;

		return sucesso ;
	}
	
	//Requisi��o assicrono pois n�o espera resposta.
	public void populaBaseDados() throws LayerException{
		BusinessDelegate.getInstance().populaBaseDados();
	}
	
	
}